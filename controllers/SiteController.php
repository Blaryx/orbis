<?php


namespace app\Controllers;

use app\models\Item;
use app\models\Map;
use app\system\Controller;
use app\system\View;

/**
 * Class SiteController
 * Working with items
 *
 * @package app\Controllers
 */
class SiteController extends Controller {

    public function actionIndex() {

        $items = Item::getItemsWithChildren();

        $this->render('index', [
            'page'      => 1,
            'items'     => $items,
            'totalRows' => Item::count(),
        ]);
    }

    public function actionDisplayTableAjax() {

        $page = $_POST['page'] ?: 1;

        $total = Item::count();
        if (ceil($total / View::LIMIT) < $page) {
            $page--;
        }
        
        $params = [
            'startFrom' => ($page - 1) * View::LIMIT,
        ];

        $items = Item::getItemsWithChildren($params);
        
        $this->render('partials/table', [
            'items'     => $items,
            'totalRows' => $total,
            'page'      => $page,
        ]);
    }

    public function actionCreateProduct() {
        $product = Item::newProduct();

        if (!$product->validate($_POST)) {
            $this->badRequest();
            echo json_encode($product->getErrors());
            return;
        }

        $product->insert([
            'name' => urldecode($_POST['name']),
            'cost' => $_POST['cost'],
        ]);
    }

    public function actionCreateSet() {
        $set = Item::newSet();

        if (!$set->validate($_POST)) {
            $this->badRequest();
            echo json_encode($set->getErrors());
            return;
        }
        $items = $set->select('*, SUM(cost) over() as total', 'id IN (' . urldecode($_POST['ids']).')');
        
        $id = $set->insert([
            'name' => urldecode($_POST['name']),
            'cost' => $items[0]['total'],
            'type' => Item::TYPE_SET,
        ]);

        if ($id) {
            foreach ($items as $item) {
                $map = new Map();
                $map->insert([
                    'parent_id' => $id,
                    'child_Id'  => $item['id']
                ]);
            }
        }
    }

    public function actionDeleteItem() {

        $id = $_POST['id'];
        
        $item = Item::getById($id);

        if ($item->hasParent()) {
            $this->badRequest();
            echo json_encode($item->getErrors());
            return;
        }

        $item->delete();

        echo json_encode(true);
    }
}