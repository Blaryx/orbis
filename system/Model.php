<?php


namespace app\system;

use app\models\Item;
use function system\dbConnect;

/**
 * Class Model
 * The base Model object
 *
 * It is not an active record object and there's no query builder, since I have not much time for doing it. But a kind of model anyway.
 */
abstract class Model extends Main {
    
    protected array $errors;
    
    public int $id;
    
    public function tableName(){
        
        $called_class = get_called_class();
        return $called_class::TABLE_NAME;
    }
    
    public static function create(array $data) {
        
        $called_class = get_called_class();
        /** @var Model $model */
        $model = new $called_class();
        return $model->insert($data);
    }

    public static function count() : int {
        
        $called_class = get_called_class();
        /** @var Model $model */
        $model = new $called_class();
        $result = mysqli_query(dbConnect(), "SELECT COUNT(*) FROM ". $model->tableName());
        return $result ? mysqli_fetch_row($result)[0] : 0;
    }
    
    
    protected function validate($data) : bool{
        return true;
    }

    public function getErrors() : array {
        return $this->errors;
    }
    
    public function select(string $fields, $where = null, $order = null, $limit = null, $startFrom = null) : array {
        
        $where = $where ? " WHERE $where" : null;
        $order = $order ? " ORDER BY $order" : null;
        $limit = $limit ? (" LIMIT " . ($startFrom ? "$startFrom, " : null) . $limit) : null;

        $result = mysqli_query(dbConnect(), "SELECT $fields FROM ". $this->tableName() . " $where $order $limit");

        $rows = [];
        while ($result && $row = mysqli_fetch_assoc($result)) {
            $rows[] = $row;
        }

        return $rows;
    }

    
    public function insert(array $data) {
        list($fields, $values) = [[], []];

        foreach ($data as $key => $value) {
            $fields[] = "`" . $key . "`";
            $values[] = "'" . $value . "'";
        }

        $fields = join(',', $fields);
        $values = join(',', $values);
        $dbc = dbConnect();

        if (mysqli_query($dbc,"INSERT INTO ". $this->tableName() . " ($fields) VALUES($values)") !== false) {
            return mysqli_insert_id($dbc);
        }
    }

    public function update(array $values, $where) : bool {
        
        $fields = [];
        foreach ($values as $key => $value) {
            
            $fields[] = "$key = '$value'";
        }
        
        $fieldsStr = join(', ', $fields);

        $result = mysqli_query(dbConnect(),"UPDATE ". $this->tableName() . " SET $fieldsStr " . ($where ? " WHERE $where" : null));
        
        return  $result !== false;
    }

    public function delete() : bool {
        
        if (empty($this->id)) {
            return false;
        }
        
        $return = mysqli_query(dbConnect(), "DELETE FROM ". $this->tableName() . " WHERE id = {$this->id}");
        
        return $return !== false;
    }
}