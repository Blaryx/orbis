<?php


namespace app\system;

/**
 * Class View
 * The base View object
 */
class View extends Main {
    
    const LIMIT = 5;

    public static function render($viewName, $params = []) {
        foreach ($params as $param => $value) {
            $$param = $value;
        }

        require("views/$viewName.php");
    }
}