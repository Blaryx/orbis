<div class="item-table-container">
    <div class="item-table__info bg-success text-light text-center"></div>
    <div class="item-table__error bg-danger text-light text-center"></div>

    <table class="item-table table" data-page="<?= $page ?>" >
        <thead class="font-weight-bold">
            <tr>
                <td></td>
                <td>#</td>
                <td>Name</td>
                <td>Type</td>
                <td>Cost</td>
                <td>Composition</td>
                <td>Actions</td>
            </tr>
        </thead>
        
        <tbody>
            <?php
            foreach ($items as $row) {
                echo 
                '<tr>',
                    "<td><input type=\"checkbox\" onclick=\"tableActions.updateIds('{$row['id']}','{$row['name']}')\"></td>",
                    "<td>{$row['id']}</td>",
                    "<td data-attr=\"name\">{$row['name']}</td>",
                    "<td data-attr=\"type\">{$row['typeStr']}</td>",
                    "<td data-attr=\"cost\">{$row['cost']}</td>",
                    "<td data-attr=\"composition\">". ($row['composition'] ?? 0) . "</td>",
                    "<td><button class=\"btn btn-danger btn-sm\" onclick=\"tableActions.deleteItem({$row['id']}, $page)\">Delete</button></td>",
                '</tr>';
            }
            ?>
        </tbody>
    </table>
</div>

<div class="text-center">
    <?php
    if ($totalRows > \app\system\View::LIMIT) {
        for ($i = 1; $i <= ceil($totalRows / \app\system\View::LIMIT); $i++) {
            if ($i != $page) {
                echo '<a class="btn btn-default" onclick="tableActions.loadTable(' . ($i) . ')">' . $i . '</a>';
            } else {
                echo '<a class="btn btn-primary">' . $i . '</a>';
            }
        }
    }
?>
</div>