<?php

use app\system\App;
use app\system\View;

?>
<div>
    <div class="create-item-form__info bg-success text-light text-center"></div>
    <div class="create-item-form__error bg-danger text-light text-center"></div>
</div>
<div class="container-fluid text-center">
    <div class="card mt-4">
        <div class="card-body">
    
            <form class="create-item-form" method="post">
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <h4>Add a new item:</h4>
                    </div>
                    <div class="form-group col-md-3">
                        <input name="name" class="form-control" placeholder="Name">
                    </div>
                    <div class="form-group col-md-3">
                        <input type="number" name="cost" class="form-control" placeholder="Cost">
                    </div>
                    <div class="form-group col-md-3">
                        <button class="btn btn-primary" onclick="tableActions.createProduct()">Add item</button>
                    </div>
                </div>
            </form>
    
            <form class="create-set-form" method="post">
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <h4>Add a new set:</h4>
                    </div>
                    <div class="form-group col-md-3">
                        <input name="name" class="form-control" placeholder="Name">
                    </div>
                    <div id="create-set-form__composition" class="form-group col-md-3">
                        Select items to add
                    </div>
                    <input id="create-set-form__input_ids" name="ids" type="hidden" value="">
                    <div class="form-group col-md-3">
                        <button class="btn btn-primary"  onclick="tableActions.createSet()">Add to set</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    
    <div class="row text-left mt-3">
        <div class="col-sm-12">

            <?php View::render('partials/table', [
                    'page'      => 1,
                    'items'     => $items,
                    'totalRows' => $totalRows,
                ]) ?>
        </div>
    </div>
</div>
