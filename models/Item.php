<?php


namespace app\models;

use app\system\Model;
use app\system\View;
use function system\dbConnect;

class Item extends Model {

    const TYPE_PRODUCT  = 0;
    const TYPE_SET      = 1;
    
    const TYPE_ARR = [
        self::TYPE_PRODUCT  => 'Product',
        self::TYPE_SET      => 'Set'
    ];

    private string $name;
    private float $cost;
    private int $type;

    private array $items;

    const TABLE_NAME = 'items';

    public function __construct($data = [])
    {
        foreach ($data as $index=>$value) {
            $this->$index = $value;
        }
    }

    public static function newProduct() {
        $item = new self();
        $item->type = self::TYPE_PRODUCT;
        
        return $item;
    }

    public static function newSet() {
        $item = new self();
        $item->type = self::TYPE_SET;

        return $item;
    }
    
    
    public function validate($data) : bool {
        $this->errors = [];

        if (empty($data['name'])) {
            $this->errors[] = 'Name cannot be empty';
        }
        
        if ($this->type === self::TYPE_PRODUCT AND (empty($data['cost']) OR !is_numeric($data['cost']))) {
            $this->errors[] = 'Price is not valid';
        }

        if ($this->type === self::TYPE_SET AND empty($data['ids'])) {
            $this->errors[] = 'Select items for set';
        }
        

        return !count($this->errors);
    }
    
    private function selectRecursive($params): array {
        
        $params['startFrom'] = $params['startFrom'] ?? 0;

        $where = isset($params['where']) ? " WHERE {$params['where']}" : null;
        $limit = " LIMIT {$params['startFrom']}, " . View::LIMIT;

        $initialQuery   = "(SELECT i.id,i.name,i.cost,i.type,1 FROM items as i GROUP BY i.id $where  $limit)";
        $recursiveQuery = 'SELECT i.id,i.name,i.cost,i.type,n+1 FROM cte as c
                            JOIN map as m ON c.id=m.parent_id
                            LEFT JOIN items as i ON m.child_id=i.id
                            WHERE n < 10';
        $cteQuery = "WITH RECURSIVE cte (id,name,cost,type,n) AS ($initialQuery UNION ALL $recursiveQuery) ";
        $finalQuery = "SELECT i.id,name,cost,type,GROUP_CONCAT(DISTINCT m.child_id) AS children FROM cte as i
                        LEFT JOIN map as m ON i.id=m.parent_id
                        GROUP BY i.id,name,cost,type";

        $orderResult    = mysqli_query(dbConnect(), $initialQuery);
        $recursiveResult = mysqli_query(dbConnect(), $cteQuery . $finalQuery);

        $rows = [];
        $order = [];

        while ($orderResult && $row = mysqli_fetch_assoc($orderResult)) {
            $order[] = $row['id'];
        }
        
        while ($recursiveResult && $row = mysqli_fetch_assoc($recursiveResult)) {
            $rows[$row['id']] = $row;
        }
        return [$rows,$order];
    }
    
    public static function getItemsWithChildren($params = []) : array {

        $item = new self();
        
        [$rows, $order] = $item->selectRecursive($params);
        $rows = $item->addMissedFields($rows);

        $return = [];
        foreach ($order as $id) {
            $return[] = $rows[$id];
        }

        return $return;
    }
    
    private function addMissedFields($items) : array {

        $this->items = $items;
        
        foreach ($items as $index => $item) {
            $this->items[$index]['typeStr'] = Item::TYPE_ARR[$item['type']];

            if ($item['type'] == self::TYPE_SET) {

                $this->items[$index]['composition'] = implode(', ',$this->getNames($index));
            }
        }
        return $this->items;
    }

    private function getNames($id) : array {

        if ($this->items[$id]['type'] == Item::TYPE_PRODUCT) {

            return [$this->items[$id]['name']];
        }

        // если уже считали для этой ветки
        if (!empty($this->items[$id]['composition'])) {
            return explode(', ',$this->items[$id]['composition']);
        }
        
        $childIds = explode(',',$this->items[$id]['children']);

        $names = [];
        foreach ($childIds as $childId) {
            $names = array_merge($names,$this->getNames($childId));
        }
        return $names;
    }
    
    public function hasParent() : bool {

        $result = mysqli_query(dbConnect(), "SELECT id FROM map WHERE child_id = {$this->id} LIMIT 1");
       
        $row = mysqli_fetch_assoc($result);

        if ($row) {
            $this->errors[] = 'Item is in a set';
            return true;
        }
        
        return false;
    }

    public static function getById($id) {

        if (!$id) return false;

        $result = mysqli_query(dbConnect(), "SELECT * FROM ". self::TABLE_NAME. " WHERE id = $id");

        if ($result) {
           $row = mysqli_fetch_assoc($result);
           if ($row) {
               return new self($row);
           }
        }
        
        return false;
    }
}