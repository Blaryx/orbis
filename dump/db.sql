CREATE TABLE `items` (
                         `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                         `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
                         `cost` decimal(10,2) NOT NULL DEFAULT '0.00',
                         `type` tinyint(1) DEFAULT '0',
                         PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `map` (
                       `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                       `parent_id` int(11) unsigned NOT NULL,
                       `child_id` int(11) unsigned NOT NULL,
                       PRIMARY KEY (`id`),
                       KEY `parent_id` (`parent_id`),
                       KEY `child_id` (`child_id`),
                       CONSTRAINT `map_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `items` (`id`) ON DELETE CASCADE,
                       CONSTRAINT `map_ibfk_2` FOREIGN KEY (`child_id`) REFERENCES `items` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;