isDefined = (v) => {
    return typeof v !== "undefined" && v !== null;
}

ajax = (url, params = {}, success, error) => {
    
    const xhr = new XMLHttpRequest();
    const queryArr = [];
    
    params.ajax = 1;

    for (let p in params)
        if (params.hasOwnProperty(p)) {
            queryArr.push(encodeURIComponent(p) + "=" + encodeURIComponent(params[p]));
        }
    
    const queryString = queryArr.join("&")
    
    xhr.open('POST', url + '?' + queryString);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function () {
        if (xhr.status === 200 && success) {
            success(xhr.response);
        } else if (error) {
            error(xhr)
        }
    };
    xhr.send(encodeURI(queryString));
}

setInfo = (value = null) => {
    document.querySelector('.create-item-form__info').innerHTML = value
    setTimeout(function() {
        document.querySelector('.create-item-form__info').innerHTML = null
    }, 5000)
};

setError = (value = null) => {
    document.querySelector('.create-item-form__error').innerHTML = value;
    setTimeout(() => {
        document.querySelector('.create-item-form__error').innerHTML = null
    }, 5000)
}

tableActions = {

    setIds: {},

    loadTable:  (page) => {
        const tableContainer = document.querySelector('.item-table-container'),
            tableArea = tableContainer.querySelector('.table');

        page = isDefined(page) ? page : tableArea.getAttribute('data-page');

        ajax('/displayTableAjax', {page}, function(response) {
            tableContainer.parentElement.innerHTML = response;
        }, function(response) {
            document.querySelector('.login-form__error').innerHTML = JSON.parse(response.responseText).join('<BR>')
        });

        event.preventDefault()
    },

    deleteItem: function(id,page) {
        
        ajax('/deleteItem', {id}, function() {

            setInfo('The item was successfully deleted');
            setError();

            tableActions.loadTable(page)
            
        }, (response) => {
            setError(JSON.parse(response.responseText).join('<BR>'));
            setInfo();
        });
    },

    updateIds: function(id,name) {

        if (!isDefined(id)) {
            this.setIds = {};
        } else {
            isDefined(this.setIds[id]) ? delete this.setIds[id] : this.setIds[id] = name;
        }

        this.updateSet()
    },

    updateSet: function () {

        const text = Object.values(this.setIds).join(', ')
        document.querySelector('#create-set-form__composition').innerHTML = text.length ? text : 'Select items to add'
        document.querySelector('#create-set-form__input_ids').value = Object.keys(this.setIds).join(',')
    },
    
    createSet: function() {
        const form = document.querySelector('.create-set-form')

        event.preventDefault()

        ajax('/createSet', {name: form['name'].value, ids: form['ids'].value}, function(response) {
            setInfo('The item was successfully added');
            setError();

            for (let input of form.querySelectorAll('input,textarea')) {
                input.value = null;
            }
            
            tableActions.updateIds();
            document.querySelectorAll('input[type=checkbox]').forEach(el => el.checked = false);

            tableActions.loadTable();

        }, function(response) {
            setError(JSON.parse(response.responseText).join('<BR>'));
            setInfo();
        });

    },

    createProduct: () => {
        const form = document.querySelector('.create-item-form')

        event.preventDefault()

        ajax('/createProduct', {name: form['name'].value, cost:form['cost'].value}, function() {
            setInfo('The item was successfully added');
            setError(null);

            for (let input of form.querySelectorAll('input,textarea')) {
                input.value = null;
            }

            tableActions.loadTable();

        }, function(response) {
            setError(JSON.parse(response.responseText).join('<BR>'));
            setInfo();
        });
    }
}